import { useState, useEffect } from "react"
import useStyles from "./styles.js"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAdd } from '@fortawesome/free-solid-svg-icons'
import { useSelector, useDispatch } from 'react-redux'
import { addTodo } from "./todoSlice"
import { createSelector } from 'reselect'
import ToDoItem from "./ToDoItem"
import {RootState, AppDispatch} from "../../app/store"


const completedTasks = createSelector(
    (state: RootState) => state.todo.list,
    (list) => list.filter((task:any) => task.completed)
  )

const uncompletedTasks = createSelector(
    (state: RootState) => state.todo.list,
    (list) => list.filter((task:any) => !task.completed)
)



export default function SideBar(){

    const classes = useStyles()
    const todoList = useSelector(uncompletedTasks)
    const todoListCompleted = useSelector(completedTasks)
    const [newToDo, setNewToDo] = useState("")
    const dispatch = useDispatch<AppDispatch>()


    const handleInput = (e:any) => {
        setNewToDo(e.target.value)
    }

    const onKeyPress = (e:any) => {
        if (e.key === "Enter") {
            dispatch(addTodo({title: newToDo, user: "Rudy"}))
        }
    }

    // Remise à zéro de l'input suite à la modification du state
    useEffect(()=>{
        setNewToDo("")
    }, [todoList.length])


    return(
        <div className={classes.toDoSideBar}>
            <div className={classes.toDoSideBar_header}>
                <span>Toutes les tâches</span>
            </div>

            {/* Input */}
            <div className={classes.toDoSideBar_inputContainer}>
                {newToDo === "" && (
                    <span className={classes.toDoSideBar_inputPlaceHolder}>
                        <FontAwesomeIcon icon={faAdd} /> &nbsp; Ajouter une tâche
                    </span>
                )}
                <input className={classes.toDoSideBar_input} onChange={handleInput} onKeyPress={onKeyPress} value={newToDo}/>
            </div>
            
            {/* Liste des tâches */}
            <div>
                {todoList.map((elem:any)=>(
                    <ToDoItem id={elem.id} title={elem.title} user={elem.user} key={elem.id} completed={false}/>
                ))}
            </div>
            
            {/* Liste des tâches complétées */}
            <div className={classes.toDoSideBar_section}>
                <span className={classes.toDoSideBar_sectionTitle}>Tâches terminées</span>
            </div>
            <div>
                {todoListCompleted.map((elem:any)=>(
                    <ToDoItem id={elem.id} title={elem.title} user={elem.user} key={elem.id} completed={true}/>
                ))}
            </div>
            
        </div>
    )
}