import React from "react"
import useStyles from "./styles.js"
import PropTypes, { InferProps } from 'prop-types'
import clsx from "clsx"
import Button from "../../shared/Button/Button"
import { useDispatch } from "react-redux"
import { AppDispatch } from "../../app/store"
import {onAssignUser} from "./todoSlice"

const ToDoModalPropTypes = {
    open: PropTypes.bool.isRequired,
    userAssigned: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
}


type ToDoModalTypes = InferProps<typeof ToDoModalPropTypes>;


const users = ["Rudy", "Romain", "Thibaut", "Maxime", "Héloïse"]

export default function ToDoAssignModal({open, id, userAssigned,onClose}:ToDoModalTypes){

    const classes = useStyles()
    const dispatch = useDispatch<AppDispatch>()

   
    
    const handleUserAssigned = (user:string) => ()=>{
        dispatch(onAssignUser({id: id, user: user}))
    }

    return(
        <div className={clsx(classes.todoAssignModal_backdrop, open && classes.todoAssignModal_open)}>
            <div className={classes.todoAssignModal_container}>
                <div className={classes.toDoAssignModal_userList}>
                    {users.map((user:string)=> (
                        <div className={clsx(classes.toDoAssignModal_userItem, userAssigned === user && classes.toDoAssignModal_userItemSelected)} onClick={handleUserAssigned(user)}>
                            <div className={classes.toDoAssignModal_userName}>
                                {user}
                            </div>
                        </div>
                    ))}
                </div>
                
                <div className={classes.todoAssignModal_buttonContainer}>
                    <Button onClick={onClose}>
                        Fermer
                    </Button>
                </div>
            </div>
        </div>
    )
}