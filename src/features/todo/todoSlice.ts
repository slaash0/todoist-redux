import { createSlice } from '@reduxjs/toolkit'
import { v4 as uuidv4 } from 'uuid';

interface ToDoList{
    list: Array<any>,
    selected: any
}

const initialState : ToDoList = {
    list: [],
    selected: null
}


export const toDoSlice = createSlice({
    
    name: 'todo',
    initialState,
    reducers: {
      addTodo: (state, action) => {
        const id:string = uuidv4()
        state.list.push({
            id: id,
            title: action.payload.title,
            user: action.payload.user,
            completed: false,
            description: ""
        })
        state.selected = id
      },
      onChangeTaskStatus: (state, action) => {
          const index:number = state.list.findIndex((elem:any)=> elem.id === action.payload.id)
          state.list[index].completed = !state.list[index].completed
      },
      onChangeTaskTitle : (state,action) =>  {
        const index:number = state.list.findIndex((elem:any)=> elem.id === action.payload.id)
        state.list[index].title = action.payload.title
      },

      onChangeTaskDescription : (state,action) =>  {
        const index:number = state.list.findIndex((elem:any)=> elem.id === action.payload.id)
        state.list[index].description = action.payload.description
      },

      onSelectTask : (state,action) => {
          const task:any = state.list.find((task:any)=> task.id === action.payload.id)
          state.selected = task.id
      },

      onAssignUser : (state,action) => {
        const index:number = state.list.findIndex((elem:any)=> elem.id === action.payload.id)
        state.list[index].user = action.payload.user
    },

    },
  })
  
  // Action creators are generated for each case reducer function
  export const { addTodo, onSelectTask, onChangeTaskStatus, onChangeTaskTitle, onChangeTaskDescription, onAssignUser } = toDoSlice.actions
  
  export default toDoSlice.reducer