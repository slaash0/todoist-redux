import React from "react"
import useStyles from "./styles"
import clsx from "clsx"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import PropTypes, { InferProps } from 'prop-types'
import {AppDispatch, RootState} from "../../app/store"
import { useDispatch, useSelector } from "react-redux"
import { onChangeTaskStatus, onSelectTask } from "./todoSlice"
import { createSelector } from "reselect"


const isItemSelected = createSelector(
    (state: RootState) => state.todo,
    (_:any, id:string) => id,
    (todo:any, id) => todo.selected === id
)


const ToDoItemPropTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired,
}

type ToDoItemTypes = InferProps<typeof ToDoItemPropTypes>;


export function ToDoItem({id,title, user, completed}: ToDoItemTypes){

    const classes = useStyles()
    const dispatch = useDispatch<AppDispatch>()
    const isSelected = useSelector((state:RootState)=> isItemSelected(state,id))

    console.log("rendu de l'item", title)

    const handleSelect = (e:any) => {
        dispatch(onSelectTask({id:id}))
    }

    const handleChangeStatus = (e:any) => {
        e.stopPropagation()
        dispatch(onChangeTaskStatus({id:id}))
    }


    return(
        <div className={clsx(classes.todoItem, isSelected && classes.todoItem_selected)} onClick={handleSelect}>
            <div className={clsx(classes.todoItem_status, completed && classes.todoItem_completed)} onClick={handleChangeStatus}>
                {completed && (<FontAwesomeIcon icon={faCheck} color="white" />)}
            </div>
            <div className={classes.todoItem_infos}>
                <span className={classes.todoItem_title}>
                    {title}
                </span>
                <span className={classes.todoItem_user}>
                    {user}
                </span>
            </div>
        </div>
    )
}

export default React.memo(ToDoItem)

