import React, {useState} from "react"
import useStyles from "./styles"
import { faAdd, faClock} from '@fortawesome/free-solid-svg-icons'
import Button from "../../shared/Button/Button"
import ToDoAssignModal from "./ToDoAssignModal"
import { useDispatch, useSelector } from "react-redux"
import { AppDispatch, RootState } from "../../app/store"
import { onChangeTaskStatus, onChangeTaskTitle,onChangeTaskDescription } from "./todoSlice"
import { createSelector } from "reselect"



const task = createSelector(
    (state: RootState) => state.todo,
    (todo:any) => todo.list.find((elem:any)=> elem.id === todo.selected)
)


export default function MainContent(){

    const classes = useStyles()
    const taskSelected = useSelector(task)
    const dispatch = useDispatch<AppDispatch>()
    const [open,setOpen] = useState(false)

    const handleChangeTitle = (e:any) => {
        dispatch(onChangeTaskTitle({id: taskSelected.id, title: e.target.value}))
    } 

    const handleDescription = (e:any) => {
        dispatch(onChangeTaskDescription({id: taskSelected.id, description: e.target.value}))

    }

    const handleOpenModal = () => {
        setOpen(!open)
    }
    

    return(
        <div className={classes.todoContent_mainContent}>
            {taskSelected && (
                <React.Fragment>
                    <div className={classes.todoContent_titleContainer}>
                  
                        <input className={classes.todoContent_titleInput} value={taskSelected.title} onChange={handleChangeTitle} disabled={taskSelected.completed}/>
                        
                        <Button onClick={()=>dispatch(onChangeTaskStatus({id: taskSelected.id}))}>
                            {taskSelected.completed ?"Marquer comme non terminé" : "Maquer comme terminée"}
                        </Button>
                        
                    </div>
                    
                    <div className={classes.todoContent_content}>
                        <div className={classes.todoContent_buttonsContainer}>
                            <Button icon={faAdd} style={{marginRight: 15}} onClick={handleOpenModal} disabled={taskSelected.completed}>
                                Attribuer à
                            </Button>
                            {/*<Button icon={faClock} onClick={()=>alert('ok')}>
                                Echéance
                            </Button>*/}
                        </div>
                        
                        {/*** Text Area */}
                        <div className={classes.todoContent_textAreaContainer}>
                            <span className={classes.todoContent_textAreatitle}>Description</span>
                            <textarea className={classes.todoContent_textArea} onChange={handleDescription} value={taskSelected.description} disabled={taskSelected.completed}/>
                        </div>
                        
                    </div>

                    <ToDoAssignModal 
                        open={open} 
                        onClose={handleOpenModal}
                        userAssigned={taskSelected.user}
                        id={taskSelected.id}
                    />

                </React.Fragment>
            )}
        </div>
    )
}