import React from "react"
import ToDoSidebar from "./ToDoSideBar"
import ToDoContent from "./ToDoContent"

export default function ToDo(){

    return(
        <React.Fragment>
            <ToDoSidebar/>
            <ToDoContent />
        </React.Fragment>
    )
}