import {createUseStyles} from 'react-jss'
import {COLORS} from "../../app/constants"

export const useStyles = createUseStyles({


    // ToDoContent

    todoContent_mainContent: {
        width: "80%",
        marginLeft: "20%",
        marginTop: 64,
        position: "relative"
    },

    todoContent_content: {
        padding: 30
    },

    todoContent_titleContainer: {
        height: 60,
        width: "100%",
        backgroundColor: "white",
        paddingLeft: 30,
        paddingRight: 30,
        boxSizing: "border-box",
        display:"flex",
        alignItems:"center",
        justifyContent: "space-between",
        borderBottom: "solid 1px #efefef"
    },

    todoContent_titleInput: {
        fontSize: 22,
        fontWeight: 300,
        color: "#959799",
        border: 0
        
    },

    todoContent_buttonsContainer: {
        height: 50,
        alignItems:"center",
        justifyContent:"flex-start",
        marginBottom: 15
    },


    todoContent_textAreaContainer:{
        backgroundColor: "white",
        padding: 30,
        //boxShadow: "0px 0px 3px 1px rgba(119, 119, 119, 0.3)",
        border: "solid 1px #e4e4e4",
        borderBottom: 0,


    },

    todoContent_textAreatitle: {
        marginBottom: 15,
    },

    todoContent_textArea: {
        width: "100%",
        height: 200,
        border: "solid 1px #e4e4e4",
        padding: 15,
        boxSizing: "border-box"
    },

    todoContent_textAreaBottom: {
        backgroundColor: "white",
        display:"flex",
        alignItems:"center",
        justifyContent:"flex-end",
        height: 60,
        paddingLeft: 30,
        paddingRight: 30,
        border: "solid 1px #e4e4e4"
    },

    todoContent_buttonSubmit: {
        backgroundColor: COLORS.primary,
        border: 0,
        textTransform: "uppercase",
        color: "white",
        height: 32,
        paddingLeft: 20,
        paddingRight: 20,
        fontSize: 12
    },



    // SIDEBAR
    toDoSideBar: {
        position: "fixed",
        backgroundColor: "white",
        width: "20%",
        height: "calc(100vh - 64px)",
        overflowY: "scroll",
        top: 64,
        flexDirection: "column",
        zIndex: 999
    },
 
    toDoSideBar_header : {
        backgroundColor: COLORS.primary,
        display : "flex",
        alignItems:"center",
        justifyContent:"flex-start",
        paddingLeft: 15,
        fontWeight: 700,
        height: 60,
       
 
        "& span" :{
             color: "white",
             fontSize: 16
        }
     },
 
 
     toDoSideBar_inputContainer: {
         position: "relative",
         height: 60,
         boxShadow: "0px 0px 3px 1px rgba(119, 119, 119, 0.3)",
     },
 
     toDoSideBar_inputPlaceHolder:{
         position: "absolute",
         height: "100%",
         width: "100%",
         zIndex: 0,
         display:"flex",
         alignItems:"center",
         justifyContent:"left",
         paddingLeft: 15,
         color: "#C4C4C4"
     },
 
     toDoSideBar_input : {
         border:0,
         paddingLeft: 15,
         paddingRight: 15,
         zIndex: 1,
         position: "absolute",
         height: "100%",
         width: "100%",
         backgroundColor: "transparent",
         fontSize: 16
     
     },
 
 
     toDoSideBar_section: {
         height: 36,
         backgroundColor: "#db443761",
         color: "white",
         display: "flex",
         width: "100%",
         paddingLeft: 15,
         alignItems:"center"
     },
 
     toDoSideBar_sectionTitle: {
         fontSize: 13,
         color: "black",
         fontWeight: 700
     },


     // TODoAssignModal.tsx
     todoAssignModal_backdrop :{
        position: "fixed",
        backgroundColor: "rgba(0,0,0,0.85)",
        display: "none",
        alignItems:"center",
        justifyContent:"center"
     },


     todoAssignModal_open : {
        display: "flex",
        width: "100vw",
        height: "100vh",
        top: 0,
        left: 0,
        zIndex: 1001
     },

     todoAssignModal_container : {
        width: 200,
        height: 200,
        backgroundColor: "white",
        paddingTop: 30,
        paddingBottom: 30,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 10,
     },

     toDoAssignModal_userList : {
         display:"flex",
         flexDirection: "column",
         position: "relative"
     },

     toDoAssignModal_userItem :{
        cursor: "pointer",
        display:"flex",
        width: "100%",
        alignItems:"center",
        height: 40,
        "&:hover":{
            backgroundColor: "whitesmoke",
        },
        
     },

     toDoAssignModal_userItemSelected :{
        backgroundColor: COLORS.primary,
        borderRadius: 10,


        "&:hover" :{
            backgroundColor: COLORS.primary,
        },

        "& $toDoAssignModal_userName" :{
            color: "white"
        }
     },

     toDoAssignModal_userName:{
        fontWeight: 500,
        fontSize: 16,
        paddingLeft:15,
        paddingRight: 15,
     },


     todoAssignModal_buttonContainer: {
         display: "flex",
         justifyContent:"flex-end",
         width: "100%",
         height: 40
     },


    // TODoItem.tsx
    todoItem: {
        height: 80,
        display: "flex",
        flexDirection: "row",
        paddingLeft: 15,
        paddingRight: 15,
        alignItems:"center",
        justifyContent:"flex-start",

        "&:hover" :{
            cursor: "pointer",
            backgroundColor: "whitesmoke"
        }
    },


    todoItem_selected: {
        backgroundColor: "#ffeded",

        "&:hover" : {
            backgroundColor: "#ffeded"
        }
    },


    todoItem_title: {
        fontSize: 14,
        fontWeight: 500,
        marginBottom: 5
    },

    todoItem_user :{
        fontWeight: 400,
        fontSize:13,
        color: "#252627"
    },

    todoItem_infos: {
        display:"flex",
        flexDirection: "column"
    },

    todoItem_status : {
        display:"flex",
        width: 40,
        height: 40,
        borderRadius: "100%",
        border: "solid 1px #979797",
        marginRight: 15,
        justifyContent:"center",
        alignItems:"center"
    },

    todoItem_completed: {
        backgroundColor: COLORS.primary,
        borderColor: COLORS.primary
    }






  })

export default useStyles
  