import React from "react";
import { createUseStyles } from "react-jss";
import { Provider } from 'react-redux'
import {store} from './app/store'
import ToDo from "./features/todo/ToDo";
import Header from "./shared/Header/Header";

const useStyles = createUseStyles({
  "@global": {
    body: {
      backgroundColor: "#F7F8F9",
      padding: 0,
      margin: 0,
      fontFamily: "Roboto"
    },
    
    a: {
      textDecoration: "underline"
    }
  }
});

function App() {

  useStyles();

  return (
    
    
    <Provider store={store}>
        <Header />
        <ToDo />
    </Provider>
  );
}

export default App;
