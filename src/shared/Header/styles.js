import {createUseStyles} from 'react-jss'


export const useStyles = createUseStyles({

    header: {
        backgroundColor: "white",
        display: "flex",
        alignItems:"center",
        justifyContent:"flex-start",
        height: 64,
        paddingLeft: 15,
        boxShadow: "0px 0px 3px 1px rgba(119, 119, 119, 0.3)",
        position: "fixed",
        width: "100vw",
        zIndex: 999,
        top: 0,
        left: 0
    },

    logo: {
        width: 120,
        height: 47
    }
  })

export default useStyles
  