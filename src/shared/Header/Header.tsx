
import React from "react"
import useStyles from "./styles.js"

export default function Header(){

    const classes = useStyles()

    return(
        <div className={classes.header}>
            <img src="assets/logo.png" alt="logo" className={classes.logo} />
        </div>
    )
}