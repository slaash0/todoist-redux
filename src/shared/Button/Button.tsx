
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import useStyles from "./styles"
import PropTypes, { InferProps } from 'prop-types'

const ButtonPropTypes = {
    onClick: PropTypes.func.isRequired,
    icon: PropTypes.any,
    children: PropTypes.any.isRequired,
    style: PropTypes.object,
    disabled: PropTypes.bool.isRequired,
}


const defaultProps = {
    disabled: false,
};
  

type ButtonTypes = InferProps<typeof ButtonPropTypes>;

export default function Button({onClick, icon, children, disabled, style}: ButtonTypes){

    const classes = useStyles()

    return(
        <button onClick={onClick} className={classes.button} style={{...style}} disabled={disabled}>
            {icon && (<span className={classes.icon}><FontAwesomeIcon icon={icon} /></span>)} {children}
        </button>
    )
}

Button.defaultProps = defaultProps