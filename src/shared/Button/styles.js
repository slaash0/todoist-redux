import {createUseStyles} from 'react-jss'

export const useStyles = createUseStyles({

button: {
    backgroundColor: "white",
    padding: 10,
    borderRadius: 20,
    border: 0,
    boxShadow: "0px 0px 3px 1px rgba(119, 119, 119, 0.3)",
    textTransform: "uppercase",
    color: "#AEAFAF",
    fontSize: 12,
    paddingRight: 15,
    cursor: "pointer",

    "&:hover" : {
      backgroundColor: "whitesmoke"
    }
},

icon: {
  borderRadius: "100%",
  border: "dashed 1px #AEAFAF",
  justifyContent:"center",
  alignItems:"center",
  width: 22,
  height: 22,
  display:"inline-flex",
  marginRight: 10
}


  })

export default useStyles
  